package com.release.nasapictures.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.release.nasapictures.net.ImageRepo;
import com.release.nasapictures.models.Image;

import java.util.List;

public class ImageViewModel extends ViewModel {


    // Image Repositary Created for API Calls
    private ImageRepo imageRepo;

    // Get Images from ImageRepo
    private MutableLiveData<List<Image>> mutableLiveData;

    /**
     * Constructor for ViewModel
     *
     */
    public ImageViewModel(){
        imageRepo = new ImageRepo();
    }

    /**
     * Get ImageList from ImageRepo
     *
     * @return
     */
    public LiveData<List<Image>> getImages() {
        if(mutableLiveData==null){
            mutableLiveData = imageRepo.requestImages();
        }
        return mutableLiveData;
    }


}


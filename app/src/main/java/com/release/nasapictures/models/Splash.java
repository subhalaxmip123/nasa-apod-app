package com.release.nasapictures.models;

public class Splash {

    // splash version
    private String splashVersion;

    public String getSplashVersion() {
        return splashVersion;
    }

    public void setSplashVersion(String splashVersion) {
        this.splashVersion = splashVersion;
    }
}

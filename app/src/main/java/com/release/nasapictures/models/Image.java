package com.release.nasapictures.models;

import android.util.Log;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.annotations.SerializedName;
import com.release.nasapictures.R;
import com.release.nasapictures.commons.UtilMethods;

import java.io.Serializable;

import static com.release.nasapictures.commons.Constants.API_DATE_FORMAT;
import static com.release.nasapictures.commons.Constants.DISPLAY_DATE_FORMAT;

public class Image implements Serializable {

    private String copyright;
    private String date;
    private String explanation;
    private String hdurl;
    private String media_type;
    private String service_version;
    private String title;
    private String url;

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateFormatted(){
        Log.v("ggg","API_DATE_FORMAT"+API_DATE_FORMAT);
        Log.v("ggg","DISPLAY_DATE_FORMAT"+DISPLAY_DATE_FORMAT);
        Log.v("ggg","DISPLAY_DATE_FORMAT"+this.date);
        return UtilMethods.changeDateFormat(this.date,API_DATE_FORMAT,DISPLAY_DATE_FORMAT);
    }

    public String getCopyRightText(){
        if (this.copyright != null && !this.copyright.matches("")){
            return "\u00a9" +" "+ this.copyright;
        }
        return this.copyright;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public String getHdurl() {
        return hdurl;
    }

    public void setHdurl(String hdurl) {
        this.hdurl = hdurl;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public String getService_version() {
        return service_version;
    }

    public void setService_version(String service_version) {
        this.service_version = service_version;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

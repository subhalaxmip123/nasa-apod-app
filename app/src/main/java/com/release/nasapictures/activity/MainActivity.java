package com.release.nasapictures.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.release.nasapictures.viewmodels.ImageViewModel;
import com.release.nasapictures.R;
import com.release.nasapictures.adapters.ImageGridAdapter;
import com.release.nasapictures.commons.MyApplication;
import com.release.nasapictures.databinding.ActivityMainBinding;
import com.release.nasapictures.models.Image;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    final String TAG = getClass().getSimpleName();

    // Image Grid Adapter
    ImageGridAdapter imageGridAdapter;

    // ImageViewModel
    private ImageViewModel imageViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Binding UI
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        // Image RecyclerView Grid Layout Manager Set
        binding.imageGrid.setHasFixedSize(true);
        binding.imageGrid.setLayoutManager(new GridLayoutManager(this,2));


        // Set Image Grid Adapter
        imageGridAdapter = new ImageGridAdapter(this);
        binding.setImageGridAdapter(imageGridAdapter);


        // Check Internet and Load data from API
        if(MyApplication.getInstance().isNetworkAvailable()) {

            // Show Progress Bar
            binding.progressBar.setVisibility(View.VISIBLE);

            imageViewModel = new ImageViewModel();
            imageViewModel.getImages().observe(this, new Observer<List<Image>>() {
                @Override
                public void onChanged(List<Image> imageModels) {
                    if (imageModels != null && !imageModels.isEmpty()) {
                        // Hide Progress Bar
                        binding.progressBar.setVisibility(View.GONE);

                        // set models to adapter
                        imageGridAdapter.addImageList(imageModels);
                        imageGridAdapter.notifyDataSetChanged();
                    }else{
                        // Hide Progress Bar
                        binding.progressBar.setVisibility(View.GONE);

                        // no data
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.no_data), Toast.LENGTH_LONG).show();
                    }
                }
            });

        }else{
            // No Internet or Network
            Toast.makeText(this, getResources().getString(R.string.no_network), Toast.LENGTH_LONG).show();
        }

    }

}
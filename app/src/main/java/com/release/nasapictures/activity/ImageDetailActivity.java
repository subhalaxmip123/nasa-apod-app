package com.release.nasapictures.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.constants.ScaleTypes;
import com.denzcoskun.imageslider.models.SlideModel;
import com.release.nasapictures.R;
import com.release.nasapictures.adapters.ImageGridAdapter;
import com.release.nasapictures.commons.MyApplication;
import com.release.nasapictures.commons.UtilMethods;
import com.release.nasapictures.databinding.ActivityImageDetailBinding;
import com.release.nasapictures.models.Image;
import com.release.nasapictures.viewmodels.ImageViewModel;

import java.util.ArrayList;
import java.util.List;

import static com.release.nasapictures.commons.Constants.SEND_DATA;

public class ImageDetailActivity extends AppCompatActivity {

    final String TAG = getClass().getSimpleName();

    // ImageModel
    private Image image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Binding UI
        ActivityImageDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_image_detail);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // retrieve data from previous activity
        image = (Image) getIntent().getSerializableExtra(SEND_DATA);

        // binding data to UI

        // Show More and Show Less Feature
       // binding.explanation.setShowingChar(150);
//        binding.explanation
//                .setAnimationDuration(500)
//                .setEllipsizedText("View More")
//                .setVisibleLines(3)
//                .setIsExpanded(false)
//                .setEllipsizedTextColor(ContextCompat.getColor(this, Color.RED));

        binding.setModel(image);


        binding.explanation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.explanation.toggle();

            }
        });

    }

    /**
     * set Image Slider
     *
     * @param view
     * @param logoUrl
     */
    @BindingAdapter({"app:image_url"})
    public static void setSliderImage(ImageSlider view, String logoUrl) {

        ArrayList<SlideModel> imageList = new ArrayList<>(); // Create image list

        if (logoUrl == null) {
            imageList.add(new SlideModel(R.drawable.splash_logo, ScaleTypes.FIT));
        } else {
            imageList.add(new SlideModel(logoUrl,ScaleTypes.FIT));
          //  imageList.add(new SlideModel(R.drawable.splash_logo, ScaleTypes.FIT));
        }

        view.setImageList(imageList, ScaleTypes.FIT);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
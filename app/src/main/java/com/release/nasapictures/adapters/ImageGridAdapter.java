package com.release.nasapictures.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.constants.ScaleTypes;
import com.denzcoskun.imageslider.models.SlideModel;
import com.release.nasapictures.R;
import com.release.nasapictures.activity.ImageDetailActivity;
import com.release.nasapictures.commons.CustomClickListener;
import com.release.nasapictures.databinding.ImageGridRowBinding;
import com.release.nasapictures.models.Image;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.release.nasapictures.commons.Constants.SEND_DATA;

public class ImageGridAdapter extends RecyclerView.Adapter<ImageGridAdapter.ImageGridViewHolder> implements CustomClickListener {

    private List<Image> imageModelList;
    private Context context;

    /**
     * set constructor
     *
     * @param ctx
     */
    public ImageGridAdapter(Context ctx) {
        imageModelList = new ArrayList<>();
        context = ctx;
    }


    /**
     * set image to imageview with image url using Glide
     *
     * @param view
     * @param logoUrl
     */
    @BindingAdapter({"app:image_url"})
    public static void loadImage(ImageView view, String logoUrl) {
        if (logoUrl == null) {
            view.setImageResource(R.drawable.splash_logo);
        } else {
            Glide
                    .with(view.getContext())
                    .load(logoUrl)
                    .centerCrop()
                    .placeholder(R.drawable.splash_logo)
                    .into(view);

        }
    }


    /**
     * Sort Custom ArrayList of Images
     *
     */
    public class CustomComparator implements Comparator<Image> {
        @Override
        public int compare(Image o1, Image o2) {
            return o2.getDate().compareTo(o1.getDate());
        }
    }

    /**
     * set imageList
     *
     * @param imageModelList
     */
    public void addImageList(List<Image> imageModelList) {
        Collections.sort(imageModelList, new CustomComparator());
        this.imageModelList = imageModelList;
    }

    /**
     * image count for reyclerview
     *
     * @return
     */
    @Override
    public int getItemCount() {
        return imageModelList != null ? imageModelList.size() : 0;
    }

    /**
     * createViewHolder
     *
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public ImageGridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ImageGridRowBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.image_grid_row, parent, false);

        return new ImageGridViewHolder(binding);
    }

    /**
     * The ViewHolder for Recyclerview to set view for each row
     *
     */
    static class ImageGridViewHolder extends RecyclerView.ViewHolder {
        private ImageGridRowBinding binding;

        ImageGridViewHolder(ImageGridRowBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.executePendingBindings();

        }

    }

    /**
     * binding each row to view holder
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ImageGridViewHolder holder, final int position) {
        holder.binding.setModel( imageModelList.get(position) );
        holder.binding.setItemClickListener(this);
    }

    /**
     * recyclerview card click handle
     *
     * @param image
     */
    public void cardClicked(Image image) {
        Intent intent = new Intent(context, ImageDetailActivity.class);
        intent.putExtra(SEND_DATA, image);
        context.startActivity(intent);

    }

}



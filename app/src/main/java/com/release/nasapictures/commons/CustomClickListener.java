package com.release.nasapictures.commons;

import com.release.nasapictures.models.Image;

/**
 * Click Listenre for Recyclerview
 *
 */
public interface CustomClickListener {

    /**
     * click of card of Recyclerview Row
     *
     * @param image
     */
    void cardClicked(Image image);
}

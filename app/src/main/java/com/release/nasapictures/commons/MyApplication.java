package com.release.nasapictures.commons;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class MyApplication extends Application {

    final String TAG = getClass().getSimpleName();

    // MyApplication instance

    private static MyApplication mInstance;

    // Retrofit instance for API Call
    private static Retrofit retrofit = null;

    @Override
    public void onCreate() {
        super.onCreate();
        // MyApplication Instance
        mInstance = this;
    }

    /**
     * constructor for MyApplication
     * @return
     */
    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    /**
     * check internet or network
     *
     * @return
     */
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = null;
        if (connectivityManager != null) {
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        }
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * create Retrofit Client instance for Api calls
     *
     * @return
     */
    public static Retrofit getRetrofitClient() {

        if (retrofit == null) {
            okhttp3.OkHttpClient client = new okhttp3.OkHttpClient.Builder().build();

            retrofit = new Retrofit.Builder()
                    .client(client)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(Constants.BASE_URL)
                    .build();
        }
        return retrofit;
    }

}

package com.release.nasapictures.commons;


public class Constants {

    // Base url for API
    public static String BASE_URL = "http://www.json-generator.com/api/json/";

    // Intent Pass Data key
    public static String SEND_DATA = "SEND_DATA";

    // The Api Date format
    public static String API_DATE_FORMAT = "yyyy-MM-dd";

    // The Display Date Format
    public static String DISPLAY_DATE_FORMAT = "dd MMM yyyy";

}

package com.release.nasapictures.commons;

import com.release.nasapictures.models.Image;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


public interface ApiInterface {

    /**
     * get Data from API
     *
     * @return
     */
    @GET("get/bUEHFuZBIO?indent=2")
    Call<List<Image>> getImages();


}

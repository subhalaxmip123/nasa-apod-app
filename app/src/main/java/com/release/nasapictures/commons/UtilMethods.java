package com.release.nasapictures.commons;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.text.TextUtils;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.DrawableRes;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;



import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class UtilMethods {



    /**
     * Used for the changing the date format as per required
     *
     * @param dateFromJSON
     * @param inputFormat
     * @param outputFormat
     * @return
     */
    public static String changeDateFormat(String dateFromJSON, String inputFormat, String outputFormat) {
        SimpleDateFormat inputDateFormat = new SimpleDateFormat(inputFormat);
        SimpleDateFormat outputDateFormat = new SimpleDateFormat(outputFormat);

        Date date = null;
        String outputStr = null;


        try {
            Log.v("ggg","outputStr"+dateFromJSON);

            date = inputDateFormat.parse(dateFromJSON);
            outputStr = outputDateFormat.format(date);
            Log.v("ggg","date"+outputStr);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputStr;
    }


}

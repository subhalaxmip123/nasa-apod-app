package com.release.nasapictures.net;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.release.nasapictures.commons.ApiInterface;
import com.release.nasapictures.commons.MyApplication;
import com.release.nasapictures.models.Image;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImageRepo {

    private final String TAG = getClass().getSimpleName();


    /**
     * MutableLiveData from API to get the Image Model List
     *
     * @return
     */
    public MutableLiveData<List<Image>> requestImages() {
        final MutableLiveData<List<Image>> mutableLiveData = new MutableLiveData<>();

        // Call Retrofit
        ApiInterface apiService =
                MyApplication.getRetrofitClient().create(ApiInterface.class);

        apiService.getImages().enqueue(new Callback<List<Image>>() {
            @Override
            public void onResponse(Call<List<Image>> call, Response<List<Image>> response) {

                // Successful Response
                if (response.isSuccessful() && response.body()!=null ) {
                    mutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Image>> call, Throwable t) {
                // Failure
            }
        });

        return mutableLiveData;
    }

}


Take Home Exercise: NASA APOD app

Done
--------------------


- Splash and App Icons are Added.
- Supported dark theme.
- Added MVVM and binding.
- Show more and less added for explanation.
- Hosted data.json file and used that api.
- Displaying HD url in Image Details Screen.



Pending
---------------------

- No test cases are written.
- Git commits are not added when created the project. It was added later.
- Supported from OS version Lollpop. But tested on Android 10.
- Supported only portrait mode. Not added code for landscape.
- Custom toolbar is not added.
- Added Image Slider with swipe in Image Detail Screen, but we are not getting multiple images from API. So it is tested with dummy images.
- Scroll More for Grid is not added as the api is not compatible.
- Not tested on Large Screens.





